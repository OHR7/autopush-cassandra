from cassandra.cqlengine.models import Model
import uuid
from cassandra.cqlengine import columns
from cassandra.cqlengine import connection
from cassandra.cqlengine.management import sync_table


class Base(Model):
    __abstract__ = True
    __keyspace__ = "autopush"


class Person(Base):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    first_name = columns.Text()
    last_name = columns.Text()

    def get_data(self):
        return {
            'id': str(self.id),
            'first_name': self.first_name,
            'last_name': self.last_name
        }


class Storage(Base):
    uaid = columns.Text(primary_key=True)
    chid = columns.Text()
    version = columns.Integer()

    __table_name__ = 'storage'


def sync_db():
    """
    Sync(Creates or updates) the tables with the cassandraDB so we can use it wih out
    missing tables Error
    Add all the tables that you will use.
    Example:
    sync_table(ModelName)
    :return: None
    """
    connection.setup(['127.0.0.1'], "cqlengine", protocol_version=3)

    print "sync"
    sync_table(Storage)


sync_db()
